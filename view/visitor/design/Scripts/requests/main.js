$('#login_form').submit(function(ev) {
    var base_link = document.getElementById('base_link').value+'login/';
    ev.preventDefault();
    var formData = new FormData(this);
    
    var rslt = make_ajax_request(base_link, formData);
    if(rslt.responseText > 0){
        reload_page();
    }
    else{
		console.log(rslt.responseText);
		new PNotify({
			title: "Error",
			text: rslt.responseText,
			type: "error"
		});
    }
});


$('#register_form').submit(function(ev) {
    var base_link = document.getElementById('base_link').value+'login/register/';
    ev.preventDefault();
    var formData = new FormData(this);
    
    var rslt = make_ajax_request(base_link, formData);
    if(rslt.responseText == 1){
        new PNotify({
			title: "Success",
			text: "User registered",
			type: "success"
		});
    }
    else{
		console.log(rslt.responseText);
		new PNotify({
			title: "Error",
			text: rslt.responseText,
			type: "error"
		});
    }
});

$('#edit_profile').submit(function(ev) {
	var base_link = document.getElementById('base_link').value+'user/update_profile/';
    ev.preventDefault();
	var formData = new FormData(this);
	
    var rslt = make_ajax_request(base_link, formData);
    if(rslt.responseText == 1){
		reload_page();
		new PNotify({
			title: "Success",
			text: "Changes Saved",
			type: "success"
		});
    }
    else{
		console.log(rslt.responseText);
		new PNotify({
			title: "Error",
			text: rslt.responseText,
			type: "error"
		});
    }
});

$('#create_trip_form').submit(function(ev) {
    var base_link = document.getElementById('base_link').value+'user/create_trip/';
	ev.preventDefault();
	// alert("Test");
    var formData = new FormData(this);
    
	var rslt = make_ajax_request(base_link, formData);
    if(rslt.responseText == 1){
		reload_page();
        new PNotify({
			title: "Success",
			text: "Trip Created",
			type: "success"
		});
    }
    else{
		console.log(rslt.responseText);
		new PNotify({
			title: "Error",
			text: rslt.responseText,
			type: "error"
		});
    }
});

function start_trip(trip_id){
	var base_link = document.getElementById('base_link').value+'user/start_trip/' + trip_id;
	if (confirm('Are you sure you want to start this trip?')) {
		var rslt = make_ajax_request(base_link, null);
		if(rslt.responseText == 1){
			reload_page();
			new PNotify({
				title: "Success",
				text: "Trip Deleted",
				type: "success"
			});
		}
		else{
			console.log(rslt.responseText);
			new PNotify({
				title: "Error",
				text: rslt.responseText,
				type: "error"
			});
		}
	}
}

function delete_trip(trip_id){
	var base_link = document.getElementById('base_link').value+'user/delete_trip/' + trip_id;
	if (confirm('Are you sure you want to delete this trip?')) {
		var rslt = make_ajax_request(base_link, null);
		if(rslt.responseText == 1){
			reload_page();
			new PNotify({
				title: "Success",
				text: "Trip Deleted",
				type: "success"
			});
		}
		else{
			console.log(rslt.responseText);
			new PNotify({
				title: "Error",
				text: rslt.responseText,
				type: "error"
			});
		}
	}
}

function make_ajax_request(source_link, formData){
	return $.ajax({
					async: false,
					url: source_link,
					dataType: 'html',
					cache: false,
					contentType: false,
					processData: false,
					data: formData,
					type: 'POST',
					beforeSend: function(){
					},
					success: function(response){
					},
					error: function (request, error) {
						console.log(arguments);
					}
				 });
}

function reload_page(){
	window.location.reload();
}