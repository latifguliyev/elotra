<!DOCTYPE html>
    <head>
        <title>Trips</title>

        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <!-- <link rel="stylesheet" type="text/css" href="CSSs/contact.css"> -->
        <!-- <link rel="stylesheet" type="text/css" href="CSSs/indexSlideShow.css"> -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>trip.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>tripCard.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>createTrip.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>response.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>dropMenu.css">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo VISITOR_ASSETS; ?>favicon.ico"> -->


        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_SLICK; ?>slick.css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_SLICK; ?>slick-theme.css"/>


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">




        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    </head>
    <body>

        <?php require_once('view/visitor/header.php'); ?>

                <div class="secondSlogan">
                        <h1>Random Trips</h1>
                        <p>Save your time by planning your <b>Trip</b> and get offers from <b>Locals</b></p>
                        <p>The community is always ready to show you</p>
                        <p>the <b>Wonders</b> of the world.</p>
                </div>

                <div class="emptyAsFuck"></div>
            </div>


            <div class="getIn">
                <div>
                        <h1>Connect People All around the World.</h1>
                </div>
            </div>



            <!-------------------------------------------------------------------------- -->

            <div class="popUpResponse">
                <div class="responseHolder">
                        <div class="responseHolderUp">
                                <h4>Are you sure?</h4>
                        </div>
                        <div class="responseHolderDown">
                            <button class="confirm">Yes</button>
                            <button class="deny">No</button>
                        </div>
                </div>
            </div>



            <div class="backEndConnection">
                <div class="calContainer">
                    <div>
                        <h3>LOCATIONS</h3>
                        <h3><?php echo $statistics['location_count']; ?></h3>
                    </div>

                    <div>
                        <h3>USERS</h3>
                        <h3><?php echo $statistics['user_count']; ?></h3>
                    </div>

                    <div>
                        <h3>TRIPS</h3>
                        <h3><?php echo $statistics['trip_count']; ?></h3>
                    </div>
                </div>
            </div>







            <div class="yourTripHeading">
                <div>
                    <h1>Random Trips</h1>
                </div>
            </div>









            <div class="tripContainer">

                <div class="slickSliderHolder">


                <?php foreach($randomTrips as $t){ ?>

                   <div>
                        <div class="tripCard" style = "background-image: linear-gradient(to top, #fa709a 0%, #fee140 100%)">
                            <div class="destName">
                                <h1 id="destNameDiv"><?php echo ($t['city']['name']); ?></h1>
                            </div>


                            <div class="tripGuiderInfo">
                                <div class="tripGuiderInfoUp">
                                    <p>Trip Info</p>
                                </div>
                                <div class="tripGuiderInfoDown">
                                    <p></p>
                                </div>
                            </div>


                            <div class="cardDateFrom">
                                <h4 id="dateFromLeft">Date From :</h4>
                                <h4 id="dateFromRight"><?php echo explode(" ", $t['date_from'])[0]; ?></h4>
                            </div>

                            <div class="cardDateTo">
                                <h4 id="dateToLeft">Date To :<span>&nbsp;&nbsp;&nbsp;&nbsp;</span></h4>
                                <h4 id="dateToRight"><?php echo explode(" ", $t['date_to'])[0]; ?></h4>
                            </div>
                        </div>
                    </div>

                <?php } ?>



                </div>


            </div>
