<?php
class FileSessionHandler
{
    function open($sessionName)
    {
        if (!is_dir(SESSION_FOLDER)) {
            mkdir(SESSION_FOLDER, 0777);
        }

        return true;
    }

    function close()
    {
        return true;
    }

    function read($id)
    {
        return (string)@file_get_contents(SESSION_FOLDER."/.sess_$id.sess");
    }

    function write($id, $data)
    {
        return file_put_contents(SESSION_FOLDER."/.sess_$id.sess", $data) === false ? false : true;
    }

    function destroy($id)
    {
        $file = SESSION_FOLDER."/.sess_$id.sess";
        if (file_exists($file)) {
            unlink($file);
        }

        return true;
    }

    function gc($maxlifetime)
    {
        foreach (glob(SESSION_FOLDER."/.sess_*") as $file) {
            if (filemtime($file) + $maxlifetime < time() && file_exists($file)) {
                unlink($file);
            }
        }

        return true;
    }
}

?>