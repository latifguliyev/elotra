<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
	<div class="page-wrapper">
		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<div class="page-logo">
					<a href="index.html">
						<img src="<?php echo ADMIN_STYLE; ?>layouts/layout/img/logo.png" alt="logo" class="logo-default" /> 
					</a>
					<div class="menu-toggler sidebar-toggler">
						<span></span>
					</div>
				</div>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
					<span></span>
				</a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown dropdown-user">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<img alt="" class="img-circle" src="<?php echo ADMIN_STYLE; ?>layouts/layout/img/avatar3_small.jpg" />
								<span class="username username-hide-on-mobile"> Nick </span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-default">														
								<li>
									<a href="<?php echo $base_link; ?>login/">
									<i class="icon-key"></i> Log Out </a>
								</li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
				</div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<div class="page-sidebar-wrapper">
					<!-- BEGIN SIDEBAR -->
					<div class="page-sidebar navbar-collapse collapse">
						<!-- BEGIN SIDEBAR MENU -->
						<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
							<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
							<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
							<li class="sidebar-toggler-wrapper hide">
								<div class="sidebar-toggler">
									<span></span>
								</div>
							</li>
							<!-- END SIDEBAR TOGGLER BUTTON -->												
							<li class="nav-item active">
								<a href="<?php echo $base_link; ?>" class="nav-link nav-toggle">
									<i class="icon-home"></i>
									<span class="title">Dashboard</span>
									<span class="selected"></span>
									<span class="arrow open"></span>
								</a>
								<!--
								<ul class="sub-menu active">
									<li class="nav-item start  open">
										<a href="" class="nav-link ">
											<i class="icon-bar-chart"></i>
											<span class="selected"></span>
											<span class="title">Dashboard 1</span>
										</a>
									</li>
								</ul>
								-->
							</li>
							<li class="heading">
								<h3 class="uppercase">Pages</h3>
							</li>
							<li class="nav-item  ">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-users"></i>
									<span class="title">Users</span>
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li class="nav-item  ">
										<a href="<?php echo $base_link; ?>users/admin" class="nav-link ">
											<i class="icon-user"></i>
											<span class="title">Admins</span>
										</a>
									</li>
									<li class="nav-item  ">
										<a href="<?php echo $base_link; ?>users/" class="nav-link ">
											<i class="icon-user"></i>
											<span class="title">Users</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="nav-item  ">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-social-dribbble"></i>
									<span class="title">General</span>
									<span class="arrow"></span>
								</a>								
							</li>											
						</ul>
						<!-- END SIDEBAR MENU -->
					</div>
					<!-- END SIDEBAR -->
				</div>
				<!-- END SIDEBAR -->
								
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
												
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<a href="index.html">Home</a>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span><?php echo $page_name_view;?></span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title"> <?php echo $page_header1; ?>
							<small><?php echo $page_header4 ?></small>
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->