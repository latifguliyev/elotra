<!DOCTYPE html>
    <head>
        <title>Map</title>

        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>requests.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>dropMenu.css">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo VISITOR_ASSETS; ?>favicon.ico"> -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>

        <?php require_once('view/visitor/header.php'); ?>
        </div>

        <div class = "TestDiv">
                <div class="frameClassSelf">
                    <div class="DivName">
                        <h2> Requests</h2> </div>
                    <div class="AllRequest">
                    <div class="RequestList">
                        <div class="RequestBox">
                        <div class="ReqSurName">
                            <h2> Elmir Guliyev Yevlakh</h2>
                            </div>
                            <div class="ReqMessage">
                                <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                </div>
                                <div class="ReqButtons">
                                    <button type="button" name="button" class="buttonAccept"> Accept </button>
                                    <button type="button" name="button" class="buttonDecline"> Decline </button>
                                    </div>
                        </div>
                        <div class="RequestBox">
                            <div class="ReqSurName">
                                <h2> Elmir Guliyev Ganja</h2>
                                </div>
                                <div class="ReqMessage">
                                    <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                    </div>
                                    <div class="ReqButtons">
                                        <button type="button" name="button" class="buttonAccept"> Accept </button>
                                        <button type="button" name="button" class="buttonDecline"> Decline </button>
                                        </div>
                            </div>
                            <div class="RequestBox">
                                <div class="ReqSurName">
                                    <h2> Elmir Guliyev Shaki</h2>
                                    </div>
                                    <div class="ReqMessage">
                                        <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                        </div>
                                        <div class="ReqButtons">
                                            <button type="button" name="button" class="buttonAccept"> Accept </button>
                                            <button type="button" name="button" class="buttonDecline"> Decline </button>
                                            </div>
                                </div>
                                <div class="RequestBox">
                                    <div class="ReqSurName">
                                        <h2> Elmir Guliyev Sumgait</h2>
                                        </div>
                                    <div class="ReqMessage">
                                            <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                            </div>
                                    <div class="ReqButtons">
                                                <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                <button type="button" name="button" class="buttonDecline"> Decline </button>
                                    </div>
                                    </div>
                                    <div class="RequestBox">
                                        <div class="ReqSurName">
                                            <h2> Elmir Guliyev Gabala</h2>
                                            </div>
                                        <div class="ReqMessage">
                                                <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                                </div>
                                        <div class="ReqButtons">
                                                    <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                    <button type="button" name="button" class="buttonDecline"> Decline </button>
                                        </div>
                                        </div>
                                        <div class="RequestBox">
                                            <div class="ReqSurName">
                                                <h2> Elmir Guliyev Tovuz</h2>
                                                </div>
                                            <div class="ReqMessage">
                                                    <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                                    </div>
                                            <div class="ReqButtons">
                                                        <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                        <button type="button" name="button" class="buttonDecline"> Decline </button>
                                            </div>
                                            </div>
                                            <div class="RequestBox">
                                                <div class="ReqSurName">
                                                    <h2> Elmir Guliyev Istanbul</h2>
                                                    </div>
                                                <div class="ReqMessage">
                                                        <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                                        </div>
                                                <div class="ReqButtons">
                                                            <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                            <button type="button" name="button" class="buttonDecline"> Decline </button>
                                                </div>
                                                </div>
                                                <div class="RequestBox">
                                                    <div class="ReqSurName">
                                                        <h2> Elmir Guliyev  Baku </h2>
                                                        </div>
                                                    <div class="ReqMessage">
                                                            <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                                            </div>
                                                    <div class="ReqButtons">
                                                                <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                                <button type="button" name="button" class="buttonDecline"> Decline </button>
                                                    </div>
                                                    </div>
                                                    <div class="RequestBox">
                                                        <div class="ReqSurName">
                                                            <h2> Elmir Guliyev  Budapest </h2>
                                                            </div>
                                                        <div class="ReqMessage">
                                                                <h5>I'm free from 10 to 6 and I guide for free! If it's suitable for you, I can pick you up from the airport and show all beautiful and historical sightseeings.</h5>
                                                                </div>
                                                        <div class="ReqButtons">
                                                                    <button type="button" name="button" class="buttonAccept"> Accept </button>
                                                                    <button type="button" name="button" class="buttonDecline"> Decline </button>
                                                        </div>
                                                        </div>



                                </div>
                            </div>
                    </div>
                  </div>
