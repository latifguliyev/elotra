                        
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Table</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#new_user"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <!-- <th><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /><span></span></label></th>
                                                -->
                            <th> Id </th>
                            <th> Name </th>
                            <th> Username</th>
                            <th> Type </th>
                            <th> Joined </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($users as $u){
                            $change_status_confirmation_function = "";
                            $delete_confirmation_function = "";
                        ?>
                        <tr class="odd gradeX">
                            <td>
                                <?php echo $u['id']; ?>
                            </td>
                            <td>
                                <?php echo $u['name']; ?>
                            </td>
                            <td>
                                <?php echo $u['username']; ?>
                            </td>
                            <td>
                                <?php
                                    echo ($u['create_admin']==1)?'Admin':'Place';
                                ?>
                            </td>
                            <td>
                                <?php echo $u['created_at']; ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a data-toggle="modal" href="#see_user_<?php echo $u['id']?>">
                                                <i class="icon-docs"></i> See full info
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- START MODALS -->
<div class="modal fade draggable-modal" id="new_user" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id='new_user_form'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">New User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="user" class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        
                                        <td style="width:15%"> Name* </td>
                                        <td style="width:50%">
                                            <input type="text" name='name' placeholder="name" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%"> Username* </td>
                                        <td style="width:50%">
                                            <input type="text" name='username' placeholder="username" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                            
                                    
                                    <tr>
                                        <td style="width:15%"> Password* </td>
                                        <td style="width:50%">
                                            <input type="password" name='password' placeholder="password" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%"> Password Repeat* </td>
                                        <td style="width:50%">
                                            <input type="password" name='password_repeat' placeholder="password" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%"> Type* </td>
                                        <td style="width:50%">
                                            <select name='create_admin' class="col-md-6 form-control" required>
                                                <option value="0">Place</option>
                                                <option value="1">Admin</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type='button' data-dismiss='modal' class='btn dark btn-outline'>Cancel</button>
                    <input type='submit' class='btn red' value='Add'>
                    <!--<button type="button" >Save</button>-->
                </div>
            </form>
        </div>
    </div>
</div>


<?php
    foreach($users as $u){
?>
    <div class="modal fade draggable-modal" id="see_user_<?php echo $u['id'] ?>" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">User Info: <?php echo $u['name']; ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="user" class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <td style="width:15%"> Name: </td>
                                            <td style="width:50%">
                                                <?php echo $u['name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%"> Register Date: </td>
                                            <td style="width:50%">
                                                <?php echo $u['created_at']; ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="width:15%"> Username: </td>
                                            <td style="width:50%">
                                                <?php echo $u['username']; ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type='button' data-dismiss='modal' class='btn dark btn-outline'>Close</button>
                        <!--<button type="button" >Save</button>-->
                    </div>
            </div>
        </div>
    </div>
<?php } ?>