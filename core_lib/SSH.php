<?php

class SSH{
	private $server_ip;
	private $sshport;
	private $user;
	private $password;

	private $ssh;

	function __construct($server_ip, $sshport, $user, $password){
		$this->server_ip = $server_ip;
		$this->sshport = $sshport;
		$this->user = $user;
		$this->password = $password;

		if(!$this->ssh = ssh2_connect($this->server_ip, $this->sshport))
		{
			throw new Exception("Error: Could not connect to the selected server. Server IP or SSH port wrong.");
		}
		if(!ssh2_auth_password($this->ssh, $this->user, $this->password)) {
			throw new Exception("Error: Could not connect to the selected server. Wrong Username or password.");
		 }
	}

	function command($cmd){
		$stream = ssh2_exec($this->ssh, $cmd);
		stream_set_blocking($stream, true);
		$data = '';
		while($buffer = fread($stream, 4096)) {
			$data .= $buffer;
		}
		fclose($stream);
		return $data;
	}

	function closeConnection(){
		ssh2_exec($this->ssh, "exit;");
		unset($this->ssh);
	}

}


?>
