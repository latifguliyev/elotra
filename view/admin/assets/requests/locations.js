$('#new_location_form').submit(function(ev) {
	var base_link = document.getElementById('base_link').value+'locations/new/';
	ev.preventDefault();
	var formData = new FormData(this);
	var dialog = bootbox.dialog({
								title: 'Loading',
								message: '<p><i class="fa fa-spin fa-spinner"></i> Creating...</p>'
								});
	var rslt = make_ajax_request(base_link, formData, dialog);
	if(rslt.responseText > 0){
		reload_page();
	}
	else{
		bootbox.alert('Error Occured: '+rslt.responseText);
	}
});

function confirm_update(in_id){
	bootbox.confirm("Are you sure to update this location?", function(result) {
		if(result){
			var base_link = document.getElementById('base_link').value+'locations/edit/';
			var editForm = document.getElementById('edit_location_form_'+in_id);
			var formData = new FormData(editForm);
			var dialog = bootbox.dialog({
										title: 'Loading',
										message: '<p><i class="fa fa-spin fa-spinner"></i> Updating...</p>'
										});
			var rslt = make_ajax_request(base_link, formData, dialog);
			if(rslt.responseText==1){
				reload_page();
			}
			else{
				bootbox.alert('Error Occured: '+rslt.responseText);
			}
		}
	});
}

function confirm_delete(in_id){
	bootbox.confirm("Are you sure to delte this location?", function(result) {
		if(result){
			var base_link = document.getElementById('base_link').value+'locations/delete/'+in_id;
			var editForm = document.getElementById('edit_location_form_'+in_id);
			// var formData = new FormData(editForm);
			var dialog = bootbox.dialog({
										title: 'Loading',
										message: '<p><i class="fa fa-spin fa-spinner"></i> Updating...</p>'
										});
			var rslt = make_ajax_request(base_link, null, dialog);
			if(rslt.responseText==1){
				reload_page();
			}
			else{
				bootbox.alert('Error Occured: '+rslt.responseText);
			}
		}
	});
}


function make_ajax_request(source_link, formData, dialog){
	return $.ajax({
					async: false,
					url: source_link,
					dataType: 'html',
					cache: false,
					contentType: false,
					processData: false,
					data: formData,
					type: 'POST',
					beforeSend: function(){
						dialog.init();
					},
					success: function(response){
						dialog.modal('hide');
					},
					error: function (request, error) {
						bootbox.alert(error);
						console.log(arguments);
					}
				 });
}

function reload_page(){
	window.location.reload();
}