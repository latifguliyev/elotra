<!DOCTYPE html>
    <head>
        <title>ABOUT US</title>

        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>about.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>AboutUs.css"> -->
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo VISITOR_ASSETS; ?>favicon.ico"> -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
        <?php require_once('view/visitor/header.php'); ?>
                    <h1> ABOUT US</h1>

            </div>

            <div class="QuoteSection">
          	       	      <div class="PhotoPlane"><img src="<?php echo VISITOR_ASSETS; ?>00.png" alt="">
                          </div>
          				   <div class="QuoteContent">
          						<div class="OurSlogan">TRAVEL TOGETHER</div>
          						<div class="Qoute">Traveling is a brutality. It forces you to trust strangers and to lose sight of all that familiar comforts of home and friends. You are constantly off balance. Nothing is yours except the essential things. -air, sleep, dreams, the sea, the sky. -all things tending towards the eternal or what we imagine of it. “The man who goes alone can start today; but he who travels with another must wait till that other is ready.” – Henry David Thoreau</div>
          				  </div>
          	</div>

            <div class="membersblock">
              <div class="ourteammesage">
                  <h2><center> OUR TEAM</center></h2>
                  </div>
                  <<div class="ourmembers">


                      <div class="firstmember">
                      <div class="figure"><img src="<?php echo VISITOR_ASSETS; ?>teammember.png" alt="" />
                        <div class="figcaption">
                          <p class="team_name">Orkhan Mammadkarimov</p>
                          <p class="team_title">Front-end developer</p>
                        </div>
                      </div>
                      </div>
                      <div class="secondmember">
                      <div class="figure"><img src="<?php echo VISITOR_ASSETS; ?>teammember.png" alt="" />
                        <div class="figcaption">
                          <p class="team_name">Latif Guliyev</p>
                          <p class="team_title">Back-end developer</p>

                        </div>
                        </div>
                        </div>
                        <div class="thirdmemmber">
                      <div class="figure"><img src="<?php echo VISITOR_ASSETS; ?>teammember.png" alt="" />
                        <div class="figcaption">
                          <p class="team_name">Elmir Guliyev</p>
                          <p class="team_title">Front-end developer / AWS</p>

                        </div>
                      </div>

                    </div>
                    </div>
                      </div>
