<?php

class UsersController extends View{

	function __construct(){
		Security::loggedIn('ADMIN_USER', 'login/', true);
	}

	function index(){
		$users = API::post_request($_SESSION['ADMIN_USER']['auth'], 'http://127.0.0.1:8000/api/admin/all_users/');
		parent::viewDoc(array('admin/template'), 
						array('page'=>'users',
							  'page_name_view'=>'Users',
							  'page_title'=>'Users', 
							  'page_header1'=>'Users Table', 
							  'page_header4'=>'List',
							  'users'=>$users
							  )
						);
	}


	function new(){
        $data = Security::checkInput($_POST);
		$data['auth'] = $_SESSION['ADMIN_USER']['auth']['auth'];
		if($data['password'] != $data['password_repeat']){
			echo "Please check password";
			return;
		}
		$response = API::post_request($data, 'http://127.0.0.1:8000/api/admin/new/');
		print_r($response);
        if(!array_key_exists("error",$response)){
            echo 1;
        }
        else{
            echo $response['error'];
        }
	}
}

?>