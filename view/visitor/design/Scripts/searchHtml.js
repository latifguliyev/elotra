$(document).ready(function(){

    $(document).mouseup(function(e) 
    {
        var guiderFormDiv = $('.sendRequest');
        
        if (!guiderFormDiv.is(e.target) && guiderFormDiv.has(e.target).length === 0) 
        {
            guiderFormDiv.hide();
        }
    });

                 
    $('.deny').click(function() {
        $('.sendRequest').fadeOut('fast');
        
    });

    $('.tripperProfile').click(function() {
        $('.sendRequest').fadeIn('fast');
    });
});
