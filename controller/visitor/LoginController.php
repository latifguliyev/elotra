<?php

class LoginController extends View{

	function index(){
		if(isset($_SESSION['USER'])){
			unset($_SESSION['USER']);
			echo '<script> location.replace("'.SystemDetails::getLink().'"); </script>';
			return;
		}
		else{
			$form_val = new FormValidation();
			$form_val->setRule('email', 'required', 'Please fill in email field!!!');
			$form_val->setRule('password', 'required', 'Please fill in password field!!!');
			$form_val_result = $form_val->validateForm();
			if($form_val_result===true){
				$result = API::post_request($_POST, 'http://127.0.0.1:8000/api/user/login/');
				if(!array_key_exists("error",$result)){
					$_SESSION['USER'] = $result;
					$_SESSION['USER']['auth'] = array('auth'=>array('access_token'=>$result['access_token'], 'id'=>$result['id']));
					// echo '<script> location.replace("'.SystemDetails::getLink().'"); </script>';
					echo 1;
				}
				else{
					echo $result['error'];
				}
			}
			else{
				echo $form_val_result;
			}
		}
	}

	function register(){
		$form_val = new FormValidation();
		$form_val->setRule('name', 'required', 'Please fill in name field!!!');
		$form_val->setRule('surname', 'required', 'Please fill in surname field!!!');
		$form_val->setRule('email', 'required', 'Please fill in email field!!!');
		$form_val->setRule('password', 'required', 'Please fill in password field!!!');
		$form_val->setRule('password_repeat', 'required', 'Please fill in password repeat field!!!');
		$form_val->setRule('gender_id', 'required', 'Please fill in gender field!!!');
		$form_val_result = $form_val->validateForm();
		if($form_val_result===true){
			if($_POST['password'] != $_POST['password_repeat']){
				echo "Please check the password";
				return;
			}
			unset($_POST['password_repeat']);
			$_POST['state_id'] = explode("_",$_POST['country_id'])[0];
			$_POST['country_id'] = explode("_",$_POST['country_id'])[1];
			$result = API::post_request($_POST, 'http://127.0.0.1:8000/api/user/register/');
			if(!array_key_exists("error",$result)){
				echo 1;
			}
			else{
				echo $result['error'];
			}
		}
		else{
			echo $form_val_result;
		}
	}
}

?>
