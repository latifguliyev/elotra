				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner"> 2018 &copy; ELOTRA
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- END FOOTER -->
	</div>
		<!-- BEGIN CORE PLUGINS -->
		
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->

		<!-- BEGIN PAGE LEVEL PLUGINS notifications -->
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS notifications-->


		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?php echo ADMIN_STYLE; ?>global/scripts/datatable.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->


		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->


		
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="<?php echo ADMIN_STYLE; ?>global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo ADMIN_STYLE; ?>pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="<?php echo ADMIN_STYLE; ?>layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>requests/users.js" type="text/javascript"></script>
		<script src="<?php echo ADMIN_STYLE; ?>requests/locations.js" type="text/javascript"></script>
		<!-- END THEME LAYOUT SCRIPTS -->

		<script>
			$(document).ready(function()
			{
				$('#clickmewow').click(function()
				{
					$('#radio1003').attr('checked', 'checked');
				});
			})
		</script>
	</body>

</html>
