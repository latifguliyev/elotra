<script>
    $('.navButton_contact').click(function() {
        window.location.href = "<?php echo $base_link."contact"; ?>";
    });

    $(".navButton_home").click(function() {
        window.location.href = "<?php echo $base_link; ?>";
    });

    $(".navButton_trip").click(function() {
        window.location.href = "<?php echo $base_link."trips"; ?>";
    });

    $(".navButton_about").click(function() {
        window.location.href = "<?php echo $base_link."about"; ?>";
    });
    
    $(".navButton_map").click(function() {
        window.location.href = "<?php echo $base_link."user/map"; ?>";
    });
</script>