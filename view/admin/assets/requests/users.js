$('#new_user_form').submit(function(ev) {
	var base_link = document.getElementById('base_link').value+'users/new/';
	ev.preventDefault();
	var formData = new FormData(this);
	var dialog = bootbox.dialog({
								title: 'Loading',
								message: '<p><i class="fa fa-spin fa-spinner"></i> Creating...</p>'
								});
	var rslt = make_ajax_request(base_link, formData, dialog);
	if(rslt.responseText > 0){
		reload_page();
	}
	else{
		bootbox.alert('Error Occured: '+rslt.responseText);
	}
});

function confirm_update_user_status(in_id, in_form_type, in_status){
	var in_operation_type = (in_status==1)?'activate':'deactivate';
	bootbox.confirm("Are you sure to "+in_operation_type+" this user?", function(result) {
		if(result){
			var base_link = document.getElementById('base_link').value+'users/change_status/';
			var formData = new FormData();
			formData.append('id', in_id);
			formData.append('form_type', in_form_type);
			formData.append('status', in_status);
			var dialog = bootbox.dialog({
										title: 'Loading',
										message: '<p><i class="fa fa-spin fa-spinner"></i> Updating...</p>'
										});
			var rslt = make_ajax_request(base_link, formData, dialog);
			if(rslt.responseText==1){
				reload_page();
			}
			else{
				bootbox.alert('Error Occured: '+rslt.responseText);
			}
		}
	});
}


function make_ajax_request(source_link, formData, dialog){
	return $.ajax({
					async: false,
					url: source_link,
					dataType: 'html',
					cache: false,
					contentType: false,
					processData: false,
					data: formData,
					type: 'POST',
					beforeSend: function(){
						dialog.init();
					},
					success: function(response){
						dialog.modal('hide');
					},
					error: function (request, error) {
						bootbox.alert(error);
						console.log(arguments);
					}
				 });
}

function reload_page(){
	window.location.reload();
}