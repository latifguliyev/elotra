<?php

class IndexController extends View{

    private $states;
	function __construct(){
        $this->states = API::get_request("http://127.0.0.1:8000/api/states/");
	}

    function index(){
		$_SESSION['USER'] = null;
		parent::viewDoc(['visitor/template'], ['page'=>'home']);
	}

   
    public function about(){
        parent::viewDoc(['visitor/template'], ['page'=>'about']);
    }

    public function contact(){
        parent::viewDoc(['visitor/template'], ['page'=>'contact']);
    }

    public function search_result(){
        parent::viewDoc(['visitor/template'], ['page'=>'search_result']);
    }

    public function trips(){
        $statistics = API::get_request("http://127.0.0.1:8000/api/statistics/");
        $randomTrips = API::get_request("http://127.0.0.1:8000/api/trips/random/ ");
        parent::viewDoc(['visitor/template'], ['page'=>'trips', 'statistics'=>$statistics, 'randomTrips'=>$randomTrips]);
    }
}

?>
