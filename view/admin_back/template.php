<?php
$base_link = SystemDetails::getLink(true);
$active_url = SystemDetails::getURL(true);
$classLinkActive = 'active open';
$classLinkItemSelected = '<span class="selected"></span>';
require_once('header.php');
require_once('sidebar.php');
require_once($page.'/main_content.php');
require_once('footer.php');
?>

<script>
function deleteRow(rowid)
{
		var row = document.getElementById(rowid);
		var table = row.parentNode;
		while ( table && table.tagName != 'TABLE' )
				table = table.parentNode;
		if ( !table )
				return;
		table.deleteRow(row.rowIndex);
}
</script>
