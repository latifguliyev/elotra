<?php

class LocationsController extends View{

    function __construct(){
		Security::loggedIn('ADMIN_USER', 'login/', true);
    }

    function index(){
        $locations = API::post_request($_SESSION['ADMIN_USER']['auth'], 'http://127.0.0.1:8000/api/admin/locations');
		parent::viewDoc(array('admin/template'), 
						array('page'=>'locations',
							  'page_name_view'=>'Locations',
							  'page_title'=>'Locations', 
							  'page_header1'=>'Locations Table', 
                              'page_header4'=>'List',
                              'locations'=>$locations
                        )
                    );
    }
    
    function new(){
        $data = Security::checkInput($_POST);
        $data['auth'] = $_SESSION['ADMIN_USER']['auth']['auth'];
        $response = API::post_request($data, 'http://127.0.0.1:8000/api/admin/locations/new/');
        if(!array_key_exists("error",$response)){
            echo 1;
        }
        else{
            echo $response['error'];
        }
    }

    function edit(){
        $data = Security::checkInput($_POST);
        $data['auth'] = $_SESSION['ADMIN_USER']['auth']['auth'];
        $response = API::post_request($data, 'http://127.0.0.1:8000/api/admin/locations/'.$data['id'].'/edit');
        if(!array_key_exists("error",$response)){
            echo 1;
        }
        else{
            echo $response['error'];
        }
    }

    function delete($id){
        $data['auth'] = $_SESSION['ADMIN_USER']['auth']['auth'];
        $data['location_id'] = $id;
        $response = API::post_request($data, 'http://127.0.0.1:8000/api/admin/locations/delete');
        if(!array_key_exists("error",$response)){
            echo 1;
        }
        else{
            echo $response['error'];
        }
    }
}

?>