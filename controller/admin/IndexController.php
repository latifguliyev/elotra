<?php

class IndexController extends View{

	function __construct(){
		Security::loggedIn('ADMIN_USER', 'login/', true);
	}

	function index(){
		self::dashboard();
	}

	function dashboard(){

		parent::viewDoc(array('admin/template'), 
							array('page'=>'dashboard', 
								'statistics'=>
									array('number_of_visitors'=>0, 
											'number_of_ingredients'=>0, 
											'number_of_meals'=>0, 
											'number_of_areas'=>0, 
											'recent_users'=>array()
										),
								'page_name_view'=>'Dashboard',
								'page_title'=>'Admin Dashboard', 
								'page_header1'=>'Admin Dashboard', 
							    'page_header4'=>'statistics, charts, recent events and reports')
							);
	}
}


?>