<?php

class Security extends View{

	public static function checkInput($input) {
		if (is_array($input)) {
			foreach($input as $key=>$value){
				if(is_array($value)){
					$input[$key] = self::checkInput($value);
				}else{
					$value = trim($value);
					$value = stripslashes($value);
					$value = htmlspecialchars($value);
					$input[$key] = $value;
				}
			}
			return $input;
		} else {
			$input = trim($input);
			$input = stripslashes($input);
			$input = htmlspecialchars($input);
			return $input;
		}
	}

	public static function formatPerm($perms) {
		$result = array();
		foreach ($perms as $perm) {
			array_push($result, $perm['url']);
		}
		return $result;
	}

	public static function checkPermission($page_name, $link, $adminPanel){
		if(!in_array($page_name, $_SESSION['PERMISSIONS'])){
			$data['page'] = 'notfound';
			die(header('Location: '.SystemDetails::getLink($adminPanel).$link));
		}
		return true;
	}

	public static function loggedIn($SESSION_NAME, $link, $adminPanel){
		if(!isset($_SESSION[$SESSION_NAME])){
			//die(parent::viewDoc(array($link)));
			die(header('Location: '.SystemDetails::getLink($adminPanel).$link));
		}
	}

}

?>
