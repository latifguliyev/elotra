
<!DOCTYPE html>
    <head>
        <title>Main</title>

        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>index.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>indexSlideShow.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>popUp.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>indexAddition.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>dropMenu.css">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="Assets/favicon.ico"> -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
        <?php require_once('view/visitor/header.php'); ?>


                <h1>TRAVEL TOGETHER</h1>
            </div>
            <div class="buscar-caja">
                <input type="text" name="" class="buscar-txt" placeholder="Find your trip / guide"/>
                <a class="buscar-btn">
                <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            </div>

       