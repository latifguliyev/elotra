<?php
define('VISITOR_STYLE', SITE_PATH.'/view/visitor/assets/');
define('ADMIN_STYLE', SITE_PATH.'/view/admin/assets/');
define('PHOTOS', SITE_PATH.'/folders/');


define('VISITOR_ASSETS', SITE_PATH.'/view/visitor/design/Assets/');
define('VISITOR_CSS', SITE_PATH.'/view/visitor/design/CSSs/');
define('VISITOR_SCRIPTS', SITE_PATH.'/view/visitor/design/Scripts/');
define('VISITOR_SLICK', SITE_PATH.'/view/visitor/design/slick/');
?>