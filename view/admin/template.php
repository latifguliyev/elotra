<?php
$base_link = SystemDetails::getLink(true);
$active_url = SystemDetails::getURL(true);
$classLinkActive = 'active';
$classLinkOpen = 'open';
require_once('header.php');
require_once('sidebar.php');
require_once($page.'/main_content.php');
require_once('footer.php');
?>
<input type='hidden' id='base_link' value='<?php echo $base_link; ?>'>

<script>
function deleteRow(rowid)
{
		var row = document.getElementById(rowid);
		var table = row.parentNode;
		while ( table && table.tagName != 'TABLE' )
				table = table.parentNode;
		if ( !table )
				return;
		table.deleteRow(row.rowIndex);
}
</script>
