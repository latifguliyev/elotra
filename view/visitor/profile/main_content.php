<!DOCTYPE html>
    <head>
        <title>Profile</title>

        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>profileEdit.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>profileEditv2.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>dropMenu.css">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo VISITOR_ASSETS; ?>favicon.ico"> -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>


        <?php require_once('view/visitor/header.php'); ?>
        </div>

            <div class = "TestDiv">
                    <div class="frameClassSelf">
                        <form class="regClassForm">
                            <!-- <div class="leftSide">
                                <div class="proImg">
                                    <div class="imgSelfSection"></div>
                                </div>
                                <div class="upImg">
                                    <button>Upload Image</button>
                                </div>
                                <div class="emptySpace">

                                </div>
                            </div> -->
                            <div class="rightSide">
                                <div class="headingFlexBox">
                                    <h1>Profile</h1>
                                </div>

                                <div class="firstLastRow">
                                    <div id = "firstLastLeftinRow">
                                        <?php //echo "<pre>";print_r($_SESSION['USER']);echo "</pre>"; ?>
                                        <input type="text" placeholder="First Name" value="<?php echo $_SESSION['USER']['name']; ?>" disabled/>
                                    </div>
                                    <div id = "firstLastRightinRow">
                                        <input type="text" placeholder="Last Name" value="<?php echo $_SESSION['USER']['surname']; ?>" disabled/>
                                    </div>
                                </div>

                                <div class="noChangeAtAll">
                                    <div class = "noCH_ noChangeTop_">
                                        <input type="email" placeholder="Email" value="<?php echo $_SESSION['USER']['email']; ?>" disabled/>
                                    </div>
                                    <div class = "noCH_ noChangeMiddle_">
                                        <input type="password" value="***********" placeholder="Password" disabled/>
                                    </div>
                                    <div class = "noCH_ noChangeBottom_">
                                        <input type="password" value="***********"  placeholder="*Password" disabled/>
                                    </div>
                                </div>

                                <div class="country_bday_">
                                    <div class = "country_bdayLeftSide">
                                        <input type="date" name="Birtdate" value="<?php echo explode(" ", $_SESSION['USER']['date_of_birth'])[0]; ?>" disabled/>
                                    </div>
                                    <div class = "country_bdayRightSide">
                                        <!-- <input type="text" name="Country" placeholder="Country" disabled/> -->
                                        <select name="country_id" disabled>
                                            <?php foreach($states as $s){ ?>
                                                <option value="<?php echo $s['id']."_".$s['country_id']; ?>" <?php echo ($_SESSION['USER']['state_id']==$s['id'])?'selected':''; ?>><?php echo $s['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="gender_payment_hour_new">
                                    <div id = "gender_payment_hour_leftSide">
                                            <select name="gender" disabled>
                                                    <option value="male" <?php echo ($_SESSION['USER']['gender_id']==1)?'selected':''; ?>>Male</option>
                                                    <option value="female" <?php echo ($_SESSION['USER']['gender_id']==2)?'selected':''; ?>>Female</option>
                                            </select>
                                    </div>
                                    <div id = "gender_payment_hour_middleSide">
                                            <input type="number" name="hourlyPayment" value="<?php echo $_SESSION['USER']['hourly_rate']; ?>" placeholder="Payment/Hour" disabled>
                                    </div>
                                    <div id = "gender_payment_hour_rightSide">
                                            <input type="number" name="minServiceDuration" min=1 value="<?php echo $_SESSION['USER']['minimum_tour_hour']; ?>" placeholder="Service Duration" disabled>
                                    </div>
                                </div>

                                <!-- <div class="buttonFormSectionAllNew">
                                    <button>Submit</button>
                                </div> -->
                            </div>
                        </form>
                    </div>
            </div>
        <!-- <div>
            <div class="comment">
                <div class ="numberofreviews">
                    <h2> 3 Reviews</h2>
                </div>
                <div class ="peoplescomment">
                    <div class="profilepicture">

                    </div>
                    <div class="nameandsurname">
                        <h2> Elmir Guliyev<h2>
                    </div>
                    <div class="commentitself">
                        <h2>Such a lovely guide! I didn't waste any second of my trip! It was really amazing and useful</h2>
                    </div>
                </div>
                <div class ="peoplescomment">
                    <div class="profilepicture">

                    </div>
                    <div class="nameandsurname">
                        <h2> Orkhan Mammadkarimov<h2>
                    </div>
                    <div class="commentitself">
                        <h2>Budapest was beautiful with Orkhan's guide! He was amazing and aware of every historical sightseeings.</h2>
                    </div>
                </div>
                <div class ="peoplescomment">
                    <div class="profilepicture">

                    </div>
                    <div class="nameandsurname">
                        <h2> Latif Guliyev<h2>
                    </div>
                    <div class="commentitself">
                        <h2>Such a technogical guy! He was able to use the technologies in a favor of our trip! I even didn't speak in English, but he did everything with the technologies!</h2>
                    </div>

                </div>
                <div class ="peoplescomment">
                    <div class="profilepicture">

                    </div>
                    <div class="nameandsurname">
                        <h2> Latif Guliyev<h2>
                    </div>
                    <div class="commentitself">
                        <h2>Such a technogical guy! He was able to use the technologies in a favor of our trip! I even didn't speak in English, but he did everything with the technologies!</h2>
                    </div>

                </div>
            <div class="newcomment">
                <form action="#" method="post">
                    <div class="addcomment">
                        <textarea name="comments" id="comments" cols="70" rows="3">
                            Add your comment here
                        </textarea>
                    </div>
                    <div class="sendbutton">
                        <button>SEND</button>
                    </div>
                </form>
            </div>
        </div> -->
</div>
