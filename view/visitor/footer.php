            <div class="footer">
                <div class="footerUp">
                        <div class="footerLogo"></div>
                            <div class="account">
                                <div>Account</div>
                                <div class = "footerRegister"> LogIn/Register</div>
                                <!-- <div>About</div> -->
                                <div class = "footerPrice">Pricing</div>
                                <!-- <div>Jobs</div> -->
                        </div>
                        <div class="info">
                            <div>Information</div>
                            <div><i class="fas fa-envelope"></i><span class="spanT"></span>Elotra@outlook.com</div>
                            <div><i class="fas fa-map-marker-alt"></i><span class="spanT"></span>Budapest 1203H</div>
                            <div><i class="fas fa-mobile"></i><span class="spanT"></span>+36 20 555 45 55</div>
                        </div>
                        <!-- <div class="resource">
                            <div>Resources</div>
                            <div>Community</div>
                            <div>Become a Partner</div>
                            <div>Our Technology</div>
                            <div>Documentation</div>
                        </div> -->
                        <div class="support">
                            <div>Support</div>
                            <div class = "footerContact">Contact Us</div>
                            <div class = "footerTerms">Terms of Use</div>
                            <div class = "footerPolicy">Privacy Policy</div>
                        </div>
                </div>
                <div class="footerDown">
                    <div class="fLeftSide">
                        <div>
                                2018&reg, The ELOTRA, All Rights Reserved
                        </div>
                    </div>
                    <div class="fRightSide">
                        <div class="fSocialLinkContainer">
                                <i class="fab fa-instagram"></i>
                                <i class="fab fa-linkedin"></i>
                                <i class="fab fa-facebook-square"></i>
                                <i class="fab fa-twitter-square"></i>
                        </div>
                    </div>
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <!-- <script src="<?php echo ADMIN_STYLE; ?>global/plugins/jquery.min.js" type="text/javascript"></script> -->
        <script type="text/javascript" src="<?php echo VISITOR_SCRIPTS; ?>pnotify.custom.min.js"></script>
        <!-- <script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> -->
        <!-- <script src="<?php echo ADMIN_STYLE; ?>global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  
        
        
        
        <script src="<?php echo VISITOR_SCRIPTS; ?>main.js"></script>
        
        <script type="text/javascript" src="<?php echo VISITOR_SLICK; ?>slick.min.js"></script>
        <script type="text/javascript" src="<?php echo VISITOR_SLICK; ?>slideShow.js"></script>
        <script type="text/javascript" src="<?php echo VISITOR_SCRIPTS; ?>tripHtml.js"></script>
        <script type="text/javascript" src="<?php echo VISITOR_SCRIPTS; ?>footer.js"></script>
<!--        <script src="<?php echo VISITOR_SCRIPTS; ?>bootbox.min.js" type="text/javascript"></script>-->
        
        <?php require_once("view/visitor/design/Scripts/links.js.php"); ?>

        <script type="text/javascript" src="<?php echo VISITOR_SCRIPTS; ?>requests/main.js"></script>

    </body>
</html>
