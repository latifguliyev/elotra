<?php

class UserController extends View{
    function __construct(){
		Security::loggedIn('USER', '', false);
    }
    
    public function map(){
        parent::viewDoc(['visitor/template'], ['page'=>'map']);
    }

    public function profile(){
        parent::viewDoc(['visitor/template'], ['page'=>'profile']);
    }

    public function edit_profile(){
        parent::viewDoc(['visitor/template'], ['page'=>'edit_profile']);
    }

    public function update_profile(){
        if(isset($_POST)){
            $_POST['state_id'] = explode("_",$_POST['country_id'])[0];
            $_POST['country_id'] = explode("_",$_POST['country_id'])[1];
            if(!empty($_POST['password'])){
                if($_POST['password'] != $_POST['password_repeat']){
                    echo "Pleace check the password";
                    return;
                }
            }
            $_POST['auth'] = $_SESSION['USER']['auth']['auth'];
            $result = API::post_request($_POST, 'http://127.0.0.1:8000/api/user/update/');
            if(!array_key_exists("error",$result)){
                $tmp = $_SESSION['USER']['auth'];
                $_SESSION['USER'] = API::get_request("http://127.0.0.1:8000/api/user/".$_POST['auth']['id']);
                $_SESSION['USER']['auth'] = $tmp;
				echo 1;
			}
			else{
				echo $result['error'];
			}
        }
    }

    public function mytrips(){
        // http://127.0.0.1:8000/api/user/1/
        // print_r($_SESSION['USER']);
        $userTrips = API::get_request("http://127.0.0.1:8000/api/user/".$_SESSION['USER']['id']."/trips/")['data'];
        $cities = API::get_request("http://127.0.0.1:8000/api/cities/");
        $guidedTrips = API::get_request("http://127.0.0.1:8000/api/user/".$_SESSION['USER']['id']."/guides/");
        $statistics = API::get_request("http://127.0.0.1:8000/api/statistics/");
        // print_r($userTrips);
        parent::viewDoc(['visitor/template'], ['page'=>'my_trips', 'trips'=>$userTrips, 'guidedTrips'=>$guidedTrips, 'cities'=>$cities, 'statistics'=>$statistics]);
    }

    public function create_trip(){
        $form_val = new FormValidation();
		$form_val->setRule('city_id', 'required', 'Please fill in city field!!!');
        $form_val->setRule('date_from', 'required', 'Please fill in From Date field!!!');
        $form_val->setRule('date_to', 'required', 'Please fill in Date To field!!!');
        $form_val_result = $form_val->validateForm();
        if($form_val_result===true){
            
            $_POST['state_id'] = explode("_",$_POST['city_id'])[1];
            $_POST['city_id'] = explode("_",$_POST['city_id'])[0];
            
            // http://127.0.0.1:8000/api/user/1/create_trip/
            $_POST['auth'] = $_SESSION['USER']['auth']['auth'];
            $result = API::post_request($_POST, 'http://127.0.0.1:8000/api/user/1/create_trip/');
            if(!array_key_exists("error",$result)){
				echo 1;
			}
			else{
				echo $result['error'];
			}
        }
        else{
            echo $form_val_result;
        }
    }

    public function delete_trip($trip_id){
        // http://127.0.0.1:8000/api/user/1/delete_trip/8
        $result = API::post_request($_SESSION['USER']['auth'], 'http://127.0.0.1:8000/api/user/1/delete_trip/'.$trip_id);
        if(!array_key_exists("error",$result)){
            echo 1;
        }
        else{
            echo $result['error'];
        }
    }

    public function start_trip($trip_id){
        // http://127.0.0.1:8000/api/trips/12/start
        $result = API::post_request($_SESSION['USER']['auth'], 'http://127.0.0.1:8000/api/trips/'.$trip_id.'/start');
        if(!array_key_exists("error",$result)){
            echo 1;
        }
        else{
            echo $result['error'];
        }
    }
}

?>