


        <div class = "upperNavBar">
            <div class = "socialLinks">
                <div class = "socialIconContainer">
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-linkedin"></i>
                    <i class="fab fa-facebook-square"></i>
                    <i class="fab fa-twitter-square"></i>
                </div>
            </div>
            <div class="searchBox">
                <div class="boxSelf">
                <input class="searchInput" type="text" >
                </div>
            </div>
            <?php if(!isset($_SESSION['USER'])){
            ?>
                <div class = "registerSection">
                    <div class="g">
                                <button class = "registerButton" value="subscibe">LogIn/Register</button>
                    </div>
                </div>
            <?php
            }else{  ?>
                <div class = "registerSection">
                    <div class="g">
                        <a href="<?php echo $base_link; ?>login/"><button class = "registerButton" value="subscibe">Logout</button></a>
                    </div>
                </div>
            <?php } ?>
        </div>


        <div class="regForm">
                <div class = "reg">
                    <form class="registerForm" id="register_form" method="POST">
                        <div class="headingForm">
                            <h1>Register</h1>
                        </div>
                        <div class="firstLast">
                            <div id = "firstLastLeft">
                                <input type="text" name ='name' placeholder="First Name" required/>
                            </div>
                            <div id = "firstLastRight">
                                <input type="text" name = 'surname' placeholder="Last Name" required/>
                            </div>
                        </div>

                        <div class="noChange">
                            <div id = "noChangeTop">
                                <input type="email" name = 'email' placeholder="Email" required/>
                            </div>
                            <div id = "noChangeMiddle">
                                <input type="password" name='password' placeholder="Password" required/>
                            </div>
                            <div id = "noChangeBottom">
                                <input type="password" name = 'password_repeat' placeholder="*Password" required/>
                            </div>
                        </div>
                        <div class="country_bday">
                            <div id = "country_bdayLeft">
                                <input type="date" name="date_of_birth" required/>
                            </div>
                            <div id = "country_bdayRight">
                                <!-- <input type="text" name="Country" placeholder="Country" required/> -->
                                <select name="country_id" required>
                                    <?php foreach($states as $s){ ?>
                                        <option value="<?php echo $s['id']."_".$s['country_id']; ?>"><?php echo $s['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="gender_payment_hour">
                            <div id = "gender_payment_hour_left">
                                    <select name="gender_id" required>
                                            <option value="1" selected>Male</option>
                                            <option value="2">Female</option>
                                    </select>
                            </div>
                            <div id = "gender_payment_hour_middle">
                                    <input type="number" min=0 name="hourly_rate" placeholder="Payment/Hour" required>
                            </div>
                            <div id = "gender_payment_hour_right">
                                    <input type="number" min=1  name="minimum_tour_hour" placeholder="Min Service Duration" required>
                            </div>
                        </div>
                        <div class="buttonForm">
                            <button type= 'submit' name ='sign_in' value = '1' >Sign Up</button>
                            <p class = "switch toLog">Already Registered?<a href = "#"> Log In</a></p>
                        </div>
                    </form>
                </div>
        </div>



        <div class="logForm">
                <div class = "log">
                    <form class="loginForm" id = 'login_form' method="POST">
                            <h2>Log In</h2>
                            <input type="email" name = 'email' placeholder="Email"/>
                            <input type="password" name = 'password' placeholder="*Password"/>
                            <button type= 'submit' name ='sign_in' value = '1' >Sign In</button>
                            <p class = "switch toReg">Not Registered?<a href = "#"> Sign Up</a></p>
                    </form>
                </div>
        </div>

        <div class="secondNavWhole">
                <div class="load"></div>
                <div class="secondNavBar">
                    <div class="logo">
                            <a href="<?php echo $base_link; ?>"><i class="fas fa-globe-americas"></i></a>
                    </div>
                    <div class="navButtons">
                        <ul class="navButtonsList">
                            <li class="navButton_home">HOME</li>
                            <li class="navButton_about">ABOUT</li>
                            <li class="navButton_contact">CONTACT</li>
                            <li class="navButton_trip">TRIP</li>
                            <?php if(isset($_SESSION['USER'])){ ?>
                                <li class="navButton_map">MAP</li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php if(isset($_SESSION['USER'])){
                    ?>
                        <div class="dropdown">
                            <button class="dropbtn"><img src="<?php echo VISITOR_ASSETS; ?>dropmenu.png" width="55" height = "55" alt=""> </img></button>
                            <div class="dropdown-content">
                                <a href="<?php echo $base_link;?>user/profile/"><?php echo $_SESSION['USER']['name'] ?> </a>
                                <a href="<?php echo $base_link;?>user/edit_profile/">Edit Profile </a>
                                <a href="<?php echo $base_link;?>user/mytrips/">My Trips </a>
                                <a href="<?php echo $base_link;?>user/#/">My Trips </a>
                                <a href="<?php echo $base_link;?>login/">Log out </a>
                            </div>
                        </div>
                    <?php
                    } ?>
                </div>
