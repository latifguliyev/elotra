<!DOCTYPE html>
    <head>
        <title>Search Result</title>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?php echo VISITOR_SCRIPTS; ?>searchHtml.js"></script>
        <!-- <script src = "https://code.jquery.com/jquery-3.3.1.min.js"></script>  -->
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>search.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>searchIngredient.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>searchRequest.css">
        <link rel="stylesheet" type="text/css" href="<?php echo VISITOR_CSS; ?>dropMenu.css">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo VISITOR_ASSETS; ?>favicon.ico"> -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    </head>
    <body>
       <?php require_once('view/visitor/header.php'); ?>
            </div>


            <div class="sendRequest">
                <form class="requestHolder">
                        <div class="requestHolderUp">
                                <h4>Do you want send request?</h4>
                        </div>
                        <div class="requstHolderMiddle">
                            <textarea name="" class="personalMessage"></textarea>
                        </div>
                        <div class="requestHolderDown">
                            <input type = "submit" class="confirm" value = "Send">
                            <input type="button" class="deny" value = "Cancel">
                        </div>
                </form>
            </div>











            <div class = "searchDiv">
                <div class="mainSeachContainer">
                        <div class="mainSearchUp">
                                <h1>Your Search Result</h1>
                        </div>


                        <div class="mainSearchDown">
                                    <div class="guiderHolder">
                                        <div class="guiderHolderUp">
                                            <h3>GUIDERS</h3>
                                        </div>

                                        <div class="guiderHolderDown">



                                            <div class="guiderProfile">
                                                <div id="guiderProfileImg">
                                                    <div></div>
                                                </div>

                                                <div id="guiderFullName">
                                                    <h6><span class="notbold">Orhan Rosenfeld</span></h6>
                                                </div>

                                                <div id="guiderPosition">
                                                    <h5><span class="notbold">Budapest</span></h5>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                    <div class="tripHolder">
                                        <div class="tripHolderUp">
                                                <h3>TRIPS</h3>
                                        </div>

                                        <div class="tripHolderDown">


                                                <div class="tripperProfile">
                                                        <div id="tripperProfileImg">
                                                                <div></div>
                                                            </div>

                                                            <div id="tripperFullName">
                                                                <h6><span class="notbold">Orhan Rosenfeld</span></h6>
                                                            </div>

                                                            <div id = "tripperDate">
                                                                <h6 id = "tripperDatefrom"><span class="notbold">11/12/2018</span></h6>
                                                                <h6 id = "tripperDateTo"><span class="notbold">20/12/2018</span></h6>
                                                            </div>

                                                            <div id="tripperPosition">
                                                                <h5><span class="notbold">Budapest</span></h5>
                                                            </div>
                                                </div>


                                        </div>
                                    </div>
                        </div>
                </div>

            </div>
