<?php

class LoginController extends View{
	function index(){
		if(isset($_SESSION['ADMIN_USER'])){
			unset($_SESSION['ADMIN_USER']);
		}
		parent::viewDoc(array('admin/login'), array());
		if(isset($_POST['submit_login']) && $_POST['submit_login']==1){
			$enc = new Encryption();
			//$_POST['password'] = $enc->encrypt($_POST['password'], KEY);
			$result = API::post_request($_POST, 'http://127.0.0.1:8000/api/admins/login/');
			if(!array_key_exists("error",$result)){
				$_SESSION['ADMIN_USER'] = $result;
				$_SESSION['ADMIN_USER']['auth'] = array('auth'=>array('access_token'=>$result['access_token'], 'id'=>$result['id']));
				echo '<script> location.replace("'.SystemDetails::getLink(true).'dashboard/"); </script>';
			}
			else{
				//(new LoginController())->index();
				Notifications::notify("Error", $result['error'], "error");
			}
		}
	}
}

?>
