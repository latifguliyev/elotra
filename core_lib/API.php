<?php

class API{

	public static function post_request($data, $url){
		$ch = curl_init($url);
		curl_setopt_array($ch, array(
		    CURLOPT_POST => TRUE,
		    CURLOPT_RETURNTRANSFER => TRUE,
		    CURLOPT_HTTPHEADER => array(
		        'Content-Type: application/json'
		    ),
		    CURLOPT_POSTFIELDS => json_encode($data)
		));
		// Send the request
		$response = curl_exec($ch);
		// echo htmlentities($response);
		// echo $response;
		// Check for errors
		if($response === FALSE){
		    die(curl_error($ch));
		}
		// Decode the response
		$responseData = json_decode($response, TRUE);
		if($responseData == null){
			return htmlentities($response);
		}
		return $responseData;
	}

	public static function get_request($url){
		$curlSession = curl_init();
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		$jsonData = json_decode(curl_exec($curlSession), TRUE);
		curl_close($curlSession);
		return $jsonData;
	}
	

}

?>