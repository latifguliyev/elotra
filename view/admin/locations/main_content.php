                        
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Table</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#new_location"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <!-- <th><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /><span></span></label></th>
                                                -->
                            <th> Id </th>
                            <th> Name </th>
                            <th> Description </th>
                            <th> Required Point</th>
                            <th> Lat </th>
                            <th> Lng </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($locations as $l){
                            $change_status_confirmation_function = "";
                            $delete_confirmation_function = "";
                        ?>
                        <tr class="odd gradeX">
                            <td>
                                <?php echo $l['id']; ?>
                            </td>
                            <td>
                                <?php echo $l['name']; ?>
                            </td>
                            <td>
                                <?php echo $l['description']; ?>
                            </td>
                            <td>
                                <?php echo $l['required_point']; ?>
                            </td>

                            <td class="center">
                                <?php echo $l['lat']; ?>
                            </td>
                            <td>
                                <?php echo $l['lng']; ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a data-toggle="modal" href="#edit_trip_<?php echo $l['id']?>">
                                                <i class="icon-docs"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="modal" onclick="confirm_delete(<?php echo $l['id'] ?>)">
                                                <i class="fa fa-close"></i> Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- START MODALS -->
<div class="modal fade draggable-modal" id="new_location" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id='new_location_form'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">New Location</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="trips" class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        
                                        <td style="width:15%"> Name* </td>
                                        <td style="width:50%">
                                            <input type="text" name='name' placeholder="Name" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%"> Description* </td>
                                        <td style="width:50%">
                                            <textarea name='description' placeholder="Description" class="col-md-6 form-control"  required></textarea>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="width:15%"> Required Point* </td>
                                        <td style="width:50%">
                                            <input type="number" step='0.1' name='required_point' placeholder="Required Point" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                            
                                    
                                    <tr>
                                        <td style="width:15%"> Lat* </td>
                                        <td style="width:50%">
                                            <input type="number" step='0.000001' name='lat' placeholder="LAT" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%"> Lng* </td>
                                        <td style="width:50%">
                                            <input type="number" step='0.000001' name='lng' placeholder="LNG" class="col-md-6 form-control" required>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type='button' data-dismiss='modal' class='btn dark btn-outline'>Cancel</button>
                    <input type='submit' class='btn red' value='Add'>
                    <!--<button type="button" >Save</button>-->
                </div>
            </form>
        </div>
    </div>
</div>


<?php
    foreach($locations as $l){
?>
    <div class="modal fade draggable-modal" id="edit_trip_<?php echo $l['id'] ?>" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Location Info: <?php echo $l['name']; ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="trip" class="table table-bordered table-striped">
                                    <form id='edit_location_form_<?php echo $l['id']; ?>'>
                                        <input type='hidden' name = 'id' value="<?php echo $l['id']; ?>">
                                        <tbody>
                                            <tr>
                                                <td style="width:15%"> Name* </td>
                                                <td style="width:50%">
                                                    <input type="text" name='name' value = "<?php echo $l['name']; ?>" placeholder="Name" class="col-md-6 form-control" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%"> Description* </td>
                                                <td style="width:50%">
                                                    <textarea name='description' placeholder="Description" class="col-md-6 form-control"  required><?php echo $l['description']; ?></textarea>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width:15%"> Required Point* </td>
                                                <td style="width:50%">
                                                    <input type="number" step='0.1' name='required_point' placeholder="Required Point" value="<?php echo $l['required_point']; ?>" class="col-md-6 form-control" required>
                                                </td>
                                            </tr>
                                                    
                                            
                                            <tr>
                                                <td style="width:15%"> Lat* </td>
                                                <td style="width:50%">
                                                    <input type="number" step='0.000001' name='lat' value="<?php echo $l['lat']; ?>" placeholder="LAT" class="col-md-6 form-control" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%"> Lng* </td>
                                                <td style="width:50%">
                                                    <input type="number" step='0.000001' name='lng' value="<?php echo $l['lng']; ?>" placeholder="LNG" class="col-md-6 form-control" required>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </form>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type='button' data-dismiss='modal' class='btn dark btn-outline'>Close</button>
                        <input type='submit' class='btn red' onclick="confirm_update(<?php echo $l['id']; ?>)" value='Save'>
                        <!--<button type="button" >Save</button>-->
                    </div>
            </div>
        </div>
    </div>
<?php } ?>