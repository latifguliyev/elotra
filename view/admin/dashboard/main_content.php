
												<!-- BEGIN DASHBOARD STATS 1-->
												<div class="row">
														<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
																<a class="dashboard-stat dashboard-stat-v2 blue" href="#">
																		<div class="visual">
																				<i class="fa fa-comments"></i>
																		</div>
																		<div class="details">
																				<div class="number">
																						<span data-counter="counterup" data-value="<?php echo $statistics['number_of_visitors']; ?>"></span>
																				</div>
																				<div class="desc"> Number of Users </div>
																		</div>
																</a>
														</div>
														<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
																<a class="dashboard-stat dashboard-stat-v2 red" href="#">
																		<div class="visual">
																				<i class="fa fa-bar-chart-o"></i>
																		</div>
																		<div class="details">
																				<div class="number">
																						<span data-counter="counterup" data-value="<?php echo $statistics['number_of_ingredients']; ?>"></span> </div>
																				<div class="desc"> Total Ingredients </div>
																		</div>
																</a>
														</div>
														<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
																<a class="dashboard-stat dashboard-stat-v2 green" href="#">
																		<div class="visual">
																				<i class="fa fa-shopping-cart"></i>
																		</div>
																		<div class="details">
																				<div class="number">
																						<span data-counter="counterup" data-value="<?php echo $statistics['number_of_meals']; ?>"></span>
																				</div>
																				<div class="desc"> Total Meals </div>
																		</div>
																</a>
														</div>
														<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
																<a class="dashboard-stat dashboard-stat-v2 purple" href="#">
																		<div class="visual">
																				<i class="fa fa-globe"></i>
																		</div>
																		<div class="details">
																				<div class="number">
																						<span data-counter="counterup" data-value="<?php echo $statistics[
																						'number_of_areas']; ?>"></span> </div>
																				<div class="desc"> Total Meal Areas </div>
																		</div>
																</a>
														</div>
												</div>
												<div class="clearfix"></div>
												<!-- END DASHBOARD STATS 1-->

												<div class="row">
														<div class="col-lg-12 col-xs-12 col-sm-12">
																<div class="portlet light bordered">
																		<div class="portlet-title">
																				<div class="caption">
																						<i class="icon-bubble font-dark hide"></i>
																						<span class="caption-subject font-hide bold uppercase">Recent Users</span>
																				</div>
																		</div>
																		<div class="portlet-body">
																				<div class="row">
																					<?php
																					foreach($statistics['recent_users'] as $u){
																					?>
																						<div class="col-md-4">
																								<!--begin: widget 1-1 -->
																								<div class="mt-widget-1">
																										<div class="mt-icon">
																												<a href="#">
																														<i class="icon-plus"></i>
																												</a>
																										</div>
																										<div class="mt-img">
																												<img src="<?php echo PHOTOS; ?>default.jpg"> </div>
																										<div class="mt-body">
																												<h3 class="mt-username"><?php echo $u['name'] .' '.$u['surname']; ?></h3>
																												<p class="mt-user-title"> <?php echo $u['email']; ?> </p>
																										</div>
																								</div>
																						</div>
																					<?php
																					}
																					?>
																				</div>
																		</div>
																</div>
																
														</div>
												</div>
										